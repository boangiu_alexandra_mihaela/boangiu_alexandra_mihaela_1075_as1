
const FIRST_NAME = "Alexandra-Mihaela";
const LAST_NAME = "Boangiu";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
function numberParser(value) {
  if(isFinite(value)  && (value >= Number.MIN_SAFE_INTEGER && value <= Number.MAX_SAFE_INTEGER))
   {
       return parseInt(value); 
   }
   else
   {
       return NaN;
   }  
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

